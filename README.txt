********************************************************************
                     D R U P A L    M O D U L E
********************************************************************
Name: Subscribe Module
Author: John VanDyk <jvandyk at iastate dot edu>
Drupal: cvs
********************************************************************
DESCRIPTION:

Subscribes to channels published by publish.module. Nodes flow
through the channel from the publishing site to the subscribing
site.

Nodes can be pushed or pulled through the channel. When you set
up a new subscription, you choose whether you will be pulling the
nodes or whether the publishing site will be pushing them out
to you.

Here is how the node transfer works in a nutshell:

1. Publishing site does a node_load() on the node to be transferred.
2. Publishing site collects some metadata about itself.
3. Publishing site encodes and sends node via XML-RPC.

4. Subscribing site receives and decodes node via XML-RPC.
5. Subscribing site unpacks metadata, consisting of

site_url   the URL of the publishing site
site_name  the site name of the publishing site

6. Subscribing site touches up the node so it will be comfortable
   in the new site. Some of the things that happen:
   
   - a node_save() is done. Note that if the subscribing site
     is set up with the same modules as the publishing site, any
     data that was added to the node during the remote
     node_load() will be taken care of during the node and
     nodeapi callbacks on the subscribing site.
   
   - remote node ID, remote UID, and remote author username are
     saved in the subscribe_node database table
     
   - any taxonomy terms that arrive with the node are taken care
     of. Because remote-to-local vocabulary mapping is set up
     when a subscription is established, the subscribe module
     knows which vocabulary to add terms to. Or it may discard
     the terms if that's what you've selected.

********************************************************************
INSTALLATION:

1. Place the entire subscribe folder in the modules folder of
   your Drupal installation.
   
2. Go to administer > modules and enable the subscribe module.

3. You should now see a "subscribe" option in your menu tree.
   Click on it and you're read to add a channel.

********************************************************************
GETTING STARTED:

Note that when you subscribe to a channel, you must specify the
URL representing the XML-RPC endpoint of the site publishing the
channel (so Drupal knows where on the internet to make the request)
and you must specify the number of the channel you will subscribe
to (so Drupal knows which channel to request from the publishing
site). If the publishing site is a Drupal site, the URL will
typically be something like http://example.com/xmlrpc.php

********************************************************************